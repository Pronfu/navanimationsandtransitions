const triggers = document.querySelectorAll('.cool > li');

  /* only need one selected here so not using selectorAll */
  const background  = document.querySelector('.dropdownBackground');

  /* for the nav */
  const nav  = document.querySelector('.top');

  function handleEnter() 
  {
     /* when the mouse enters then do this */
    this.classList.add('trigger-enter');

    /* never showing the content of the nav too early if someone is hovering between mutliple nav items */
    setTimeout(() => 
    {
      if(this.classList.contains('trigger-enter'))
      {
        this.classList.add('trigger-enter-active')
      }
      /* after 150 millseconds then show the classList, have to do it this way so this gets from the function */
    }, 150);

    /* This is the code that Wes uses to make it shorter but I'm keeping the if so I can know in my head how it's done. 
    setTimeout(() => this.classList.contains('trigger-enter') && this.classList.add('trigger-enter-active'), 150); */

    /* show the background on the page */
    background.classList.add('open');

    /* need to figure out what the dropdown is, so the background can be shown at the exact height and width as needed */
    const dropdown = this.querySelector('.dropdown');
    const dropdownCoords = dropdown.getBoundingClientRect();

    /* get coords for the nav, so that if something is added or change it doesn't mess it all up */
    const navCoords = nav.getBoundingClientRect();

     /* where are the coords for everything and put them together */
    const coords = 
    {
      height: dropdownCoords.height,
      width: dropdownCoords.width,
      top: dropdownCoords.top - navCoords.top,
      left: dropdownCoords.left - navCoords.left
    };

    background.style.setProperty('width', `${coords.width}px`);
    background.style.setProperty('height', `${coords.height}px`);
    background.style.setProperty('transform', `translate(${coords.left}px, ${coords.top}px)`);
  }

  function handleLeave() 
  {
    /* when the mouse leaves then do this  */
    this.classList.remove('trigger-enter', 'trigger-enter-active');

    /* hide the background on the page */
    background.classList.remove('open');
  }

  /* when the mouse enter run the handleEnter function */
  triggers.forEach(trigger => trigger.addEventListener('mouseenter', handleEnter));

  /* when the mouse leaves run the handleLeave function */
  triggers.forEach(trigger => trigger.addEventListener('mouseleave', handleLeave));