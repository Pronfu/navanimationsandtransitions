# Navigation Animations And Transitions

Have you seen those fancy navigations on a website and wondered how it was done? Now you can, with some JavaScript, CSS, and HTML.

## Live Demo

[projects.gregoryhammond.ca/navanimationsandtransitions/](https://projects.gregoryhammond.ca/navanimationsandtransitions/)

  
## Acknowledgements

Based on [JavaScript30 # 26](https://javascript30.com/)

  
## License

[Unlicense](https://unlicense.org/)